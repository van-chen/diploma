import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtCore import QTimer, Qt, QPoint
from PyQt5.QtGui import QPainter, QColor, QPen, QFont
import random
import copy
import keyboard


class game_over(Exception):
    def __init__(self):
        super().__init__()


class snake:
    def __init__(self, maxx, maxy, segments):
        self.maxx = maxx
        self.maxy = maxy
        x = random.randrange(5, maxx, 10)
        y = random.randrange(5, maxy, 10)
        self._points = [QPoint(x-i*10, y) for i in range(segments)]
        self._direction = "down"

    def draw(self, qp):
        for p in self._points:
            qp.drawRect(p.x()-5, p.y()-5, 10, 10)

    def move(self, prise):
        if self._direction == "right":
            new_pos = copy.deepcopy(self._points[0])
            new_pos.setX(new_pos.x()+10)
        if self._direction == "left":
            new_pos = copy.deepcopy(self._points[0])
            new_pos.setX(new_pos.x()-10)
        if self._direction == "up":
            new_pos = copy.deepcopy(self._points[0])
            new_pos.setY(new_pos.y()-10)
        if self._direction == "down":
            new_pos = copy.deepcopy(self._points[0])
            new_pos.setY(new_pos.y()+10)

        if new_pos.x() >= self.maxx or new_pos.x() <= 0:
            raise game_over
        if new_pos.y() >= self.maxy or new_pos.y() <= 0:
            raise game_over

        self._points.insert(0, new_pos)
        if prise.x == new_pos.x() and prise.y == new_pos.y():
            prise.make_new()
        else:
            del self._points[-1]

    def change_direction(self, new_direction):
        self._direction = new_direction


class treasure:
    def __init__(self, maxx, maxy):
        self.maxx = maxx
        self.maxy = maxy
        self.x = random.randrange(5, self.maxx, 10)
        self.y = random.randrange(5, self.maxy, 10)

    def draw(self, qp):
        qp.drawRect(self.x-5, self.y-5, 10, 10)

    def make_new(self):
        self.x = random.randrange(5, self.maxx, 10)
        self.y = random.randrange(5, self.maxy, 10)


class board(QWidget):
    def __init__(self):
        super().__init__()
        self.is_game_over = False
        self.resize(300, 300)
        self.s1 = snake(300, 300, 3)
        self.prise = treasure(300, 300)
        keyboard.on_press_key(
            "left arrow", lambda _: self.s1.change_direction("left"))
        keyboard.on_press_key(
            "right arrow", lambda _: self.s1.change_direction("right"))
        keyboard.on_press_key(
            "down arrow", lambda _: self.s1.change_direction("down"))
        keyboard.on_press_key(
            "up arrow", lambda _: self.s1.change_direction("up"))

        self.timer = QTimer(self)
        self.timer.setInterval(200)
        self.timer.timeout.connect(self.update_snake)
        self.timer.start()

    def update_snake(self):
        try:
            self.s1.move(self.prise)
        except game_over:
            self.is_game_over = True
        self.update()

    def print_game_over(self, event, qp):
        qp.setPen(Qt.red)
        qp.setFont(QFont('Decorative', 10))
        qp.drawText(event.rect(), Qt.AlignCenter, "Game Over")

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        if self.is_game_over:
            self.print_game_over(event, painter)
        else:
            self.s1.draw(painter)
            self.prise.draw(painter)
        painter.end()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = board()
    w.resize(300, 300)
    w.show()

    sys.exit(app.exec_())
