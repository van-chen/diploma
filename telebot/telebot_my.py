import telebot
import random
import arithmetic
from telebot import types

bot = telebot.TeleBot("1280010608:AAHE-fS2kf2U7HYF5E9ayeZYoSfHtpUdDMY")

# bot name: my_test_bot_341

maxint = 10
user_magic_number = {}

game_board = types.InlineKeyboardMarkup(row_width=5)
game_board.add(*[types.InlineKeyboardButton(text=str(i),
                                            callback_data=str(i)) for i in range(1, maxint+1)])


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    global user_magic_number
    global maxint
    bot.send_message(message.chat.id, "Я загадал число от 1 до {}, угадай какое?".format(
        maxint), reply_markup=game_board)
    user_magic_number[message.chat.id] = random.randint(1, maxint)
    bot.send_message(
        message.chat.id, "\nЕще я умею вычислять простые арифметические выражения без скобок. Например: 1+2*3/4")


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    try:
        bot.reply_to(message, "="+str(arithmetic.arithmetic(message.text)))
    except arithmetic.CalcError:
        bot.reply_to(
            message, "Я понимаю только арифметические выражения без скобок. Например: 1+2*3")


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global user_magic_number
    bot.answer_callback_query(
        callback_query_id=call.id, text="Потерпи, кожанный мешок, я думаю!")
    if call.data == str(user_magic_number[call.message.chat.id]):
        bot.send_message(call.message.chat.id, "Ты че, экстрасенс!??")
        bot.send_message(
            call.message.chat.id, "Давай еще раз! Тебе просто повезло...", reply_markup=game_board)
    else:
        bot.send_message(
            call.message.chat.id, "Не угадал! Попробуй еще раз...", reply_markup=game_board)


bot.polling()
