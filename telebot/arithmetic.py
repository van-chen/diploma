
def make_sum(first, second):
    return first+second


def make_minus(first, second):
    return first-second


def make_multiply(first, second):
    return first*second


def make_divide(first, second):
    return first / second


functions = {'+': make_sum, '-': make_minus,
             '*': make_multiply, "/": make_divide}


class CalcError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
    """

    def __init__(self, message):
        self.message = message


class oper:
    val = ''
    action = ''
    is_skip = False

    def __init__(self, value, action):
        if len(str(value)) == 0:
            self.val = float(0)
        else:
            self.val = float(value)
        self.action = action

    def skip(self):
        self.is_skip = True

    def make_action(self, left_val):
        if self.is_skip:
            return left_val
        if self.action == '':
            return self.val
        else:
            return functions[self.action](float(left_val), self.val)


def parse(string):
    items = []
    current_val = ''
    current_action = ''
    is_first = True
    for it in string:
        if it == '+' or it == '-' or it == '*' or it == '/':
            if(current_val != ''):
                items.append(oper(current_val, current_action))
                current_action = it
                current_val = ''
            elif is_first is False:
                raise CalcError("Value expected "+it)
        else:
            if (str(it).isdigit() is False):
                raise CalcError("Unexpected symbol "+it)
            current_val += it
        is_first = False
    items.append(oper(current_val, current_action))
    return items


def arithmetic(val):
    """Simple calculation function.

    Attributes:
        val -- input string to calculate without brackets. Example '1+2*3/2'.
    """
    result = 0
    items = parse(val)
    i = 0
    while i < len(items):
        if items[i].action == '*' or items[i].action == '/':
            items[i-1].val = items[i].make_action(items[i-1].val)
            del items[i]
        else:
            i += 1
    for it in items:
        result = it.make_action(result)
    return result


if (__name__ == "__main__"):
    print(arithmetic(input("Enter operation: ")))
